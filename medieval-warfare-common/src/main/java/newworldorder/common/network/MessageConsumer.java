package newworldorder.common.network;

public interface MessageConsumer {

	public void consumeMessages() throws Exception;

}

package newworldorder.client.model;

/**
 * PlayerStatus enumeration definition.
 * Generated by the TouchCORE code generator.
 */
public enum PlayerStatus {
    OFFLINE,
    ONLINE
}

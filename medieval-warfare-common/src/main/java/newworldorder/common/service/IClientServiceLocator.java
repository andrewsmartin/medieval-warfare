package newworldorder.common.service;

public interface IClientServiceLocator {
	public IGameLauncher getGameLauncher();
}

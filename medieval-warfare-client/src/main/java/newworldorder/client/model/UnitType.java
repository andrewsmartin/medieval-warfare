package newworldorder.client.model;

/**
 * UnitType enumeration definition.
 * Generated by the TouchCORE code generator.
 */
public enum UnitType {
    PEASANT,
    INFANTRY,
    SOLDIER,
    KNIGHT
}

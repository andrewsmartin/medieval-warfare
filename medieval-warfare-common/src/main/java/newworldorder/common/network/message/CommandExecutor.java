package newworldorder.common.network.message;

public interface CommandExecutor {
	public void execute(AbstractCommand command);
}
